package in.silentsudo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StandAloneService {
    public static void main(String[] args) {
        SpringApplication.run(StandAloneService.class);
    }
}