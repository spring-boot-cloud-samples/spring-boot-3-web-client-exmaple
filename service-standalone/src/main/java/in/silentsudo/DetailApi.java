package in.silentsudo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/details")
public class DetailApi {

    @GetMapping
    public Map<String, Object> get() {
        HashMap<String, Object> additionalDetails = new HashMap<String, Object>();
        ArrayList<String> array = new ArrayList<String>();
        additionalDetails.put("version", 1);
        array.add("ABC.com");
        additionalDetails.put("vendors", array);
        return additionalDetails;
    }
}
