package in.silentsudo.simpelwebclientmicroservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpelWebClientMicroservicesApplication {

    public static void main(String[] args) {
        SpringApplication.run(SimpelWebClientMicroservicesApplication.class, args);
    }

}
