package in.silentsudo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class WebClientConfig {

    @Bean
    @Primary
    public WebClient webClient(WebClient.Builder builder) {
        return builder
                .baseUrl("http://localhost:8091")
                .build();
    }
}
