package in.silentsudo;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.Map;

@RestController
@RequestMapping("/caller")
public class CallerApi {

    private final WebClient webClient;

    public CallerApi(WebClient webClient) {
        this.webClient = webClient;
    }

    @GetMapping
    public Map<String, Object> get() {
        final Map<String, Object> details = webClient.get()
                .uri("/details")
                .retrieve()
                .bodyToFlux(new ParameterizedTypeReference<Map<String, Object>>() {
                })
                .blockFirst();
        if (details != null) {
            return Map.of("whoAmI", "caller", "details", details);
        } else {
            return Map.of("whoAmI", "caller");
        }
    }

}
